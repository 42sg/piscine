
/*
Consider the binary representation of powers of 2:
1 - 0001
2 - 0010
4 - 0100
8 - 1000
...

Only one bit is on.
The following algorithm counts the number of "on" bits by iteratively checking if the last bit is 1, and then shifting the current n right one place, i.e. removing the last bit, until 0.
*/
int	is_power_of_2(unsigned int n)
{
	int	on_bits;

	on_bits = 0;
	while (n != 0)
	{
		if (n & 1)
			on_bits++;
		n = n >> 1;
	}
	return (on_bits == 1);
}

#include <stdio.h>
int	main(void)
{
	printf("%d\n", is_power_of_2(0));
	printf("%d\n", is_power_of_2(1));
	printf("%d\n", is_power_of_2(2));
	printf("%d\n", is_power_of_2(4));
	printf("%d\n", is_power_of_2(6));
	printf("%d\n", is_power_of_2(8));
	printf("%d\n", is_power_of_2(10));
	printf("%d\n", is_power_of_2(12));
}
