#include <unistd.h>
#include "ft_atoi.c"

void	write_hex(int i)
{
	if (i > 15 || i < -15)
		write_hex(i / 16);
	i %= 16;
	if (i < 0)
		i *= -1;
	write(1, &"0123456789abcdef"[i], 1);
}

int	main(int argc, char **argv)
{
	int	val;

	if (argc == 2)
	{
		val = atoi(argv[1]);
		write_hex(val);
	}
}
