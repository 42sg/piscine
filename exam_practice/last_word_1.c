#include <unistd.h>

int	is_space(char c)
{
	return (c == 11 || c == 13 || c == 32);
}

int	main(int argc, char **argv)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	if (argc == 2)
	{
		while (argv[1][i] && argv[1][i + 1])
		{
			if (is_space(argv[1][i]) && !is_space(argv[1][i + 1]))
				j = i + 1;
			i++;
		}
		while (argv[1][j] && !is_space(argv[1][j]))
		{
			write(1, &argv[1][j], 1);
			j++;
		}
	}
	write(1, "\n", 1);
}
