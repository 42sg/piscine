#include "ft_atoi.c"
#include <unistd.h>

void	putnbr(int n)
{
	if (n > 9)
		putnbr(n / 10);
	n %= 10;
	write(1, &"0123456789"[n], 1);
}

int	main(int argc, char **argv)
{
	int	i;
	int	j;
	int	k;
	int	gcd;

	gcd = 1;
	if (argc == 3)
	{
		k = 2;
		i = atoi(argv[1]);
		j = atoi(argv[2]);
		while (k <= i && k <= j)
		{
			if (i % k == 0 && j % k == 0)
				gcd = k;
			k++;
		}
		putnbr(gcd);
	}
}
