#include <unistd.h>
#include "ft_atoi.c"

void	putnbr(int nb)
{
	if (nb > 9)
		putnbr(nb / 10);
	nb %= 10;
	write(1, &"0123456789"[nb], 1);
}

int	main(int argc, char **argv)
{
	int	val;
	int	i;

	if (argc == 2)
	{
		i = 1;
		val = atoi(argv[1]);
		while (i < 10)
		{
			putnbr(i);
			write(1, " x ", 3);
			putnbr(val);
			write(1, " = ", 3);
			putnbr(i * val);
			write(1, "\n", 1);
			i++;
		}
	}
	else
		write(1, "\n", 1);
}
