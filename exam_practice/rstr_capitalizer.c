#include <unistd.h>

int	is_space(char c)
{
	return (c == 11 || c == 13 || c == 32);
}

void	put_char(char c)
{
	write(1, &c, 1);
}

void	determine_char(char c, int to_lower)
{
	if (c >= 'A' && c <= 'Z' && to_lower)
		put_char(c + 32);
	else if (c >= 'a' && c <= 'z' && !to_lower)
		put_char(c - 32);
	else
		put_char(c);
}

void	capitalizer(char *str)
{
	while (*str)
	{
		while (*str && is_space(*str))
		{
			determine_char(*str, 0);
			str++;
		}
		while (*str && *(str + 1) && !is_space(*(str + 1)))
			determine_char(*str++, 1);
		while (*str && (is_space(*(str + 1)) || (*(str + 1) == 0)))
			determine_char(*str++, 0);
	}
	put_char('\n');
}

int	main(int argc, char **argv)
{
	while (*++argv)
		capitalizer(*argv);
	if (argc == 1)
		write(1, "\n", 1);
}
