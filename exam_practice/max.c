int	max(int *tab, unsigned int len)
{
	unsigned int	i;
	int	max_so_far;

	if (len == 0)
		return (0);
	i = 0;
	max_so_far = tab[i];
	while (i < len)
	{
		if (tab[i] > max_so_far)
			max_so_far = tab[i];
		i++;
	}
	return (max_so_far);
}

#include <stdio.h>
int	main(void)
{
	printf("%d\n", max((int[5]){-5, 4, 2, 6, 2}, 5));
	printf("%d\n", max((int[0]){}, 0));
}
