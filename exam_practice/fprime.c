#include <unistd.h>
#include "ft_atoi.c"

void	put_nbr(int n)
{
	if (n > 9)
		put_nbr(n / 10);
	n %= 10;
	write(1, &"0123456789"[n], 1);
}

int	is_prime(int n)
{
	int	i;

	if (n < 2)
		return (0);
	if (n == 2 || n == 3)
		return (1);
	i = 2;
	while (i <= n / i)
	{
		if (n % i == 0)
			return (0);
		i++;
	}
	return (1);
}

int	main(int argc, char **argv)
{
	int	val;
	int	p;

	p = 2;
	if (argc == 2)
	{
		val = atoi(argv[1]);
		if (val == 1)
			write(1, "1", 1);
		while (p <= val)
		{
			if (is_prime(p) && val % p == 0)
			{
				put_nbr(p);
				val /= p;
				if (val != 1)
					write(1, "*", 1);
			}
			else
				p++;
		}
	}
	write(1, "\n", 1);
}
