#include <unistd.h>
#include "ft_atoi.c"

void	ft_putnbr_recur(int nb)
{
	if (nb > 9 || nb < -9)
		ft_putnbr_recur(nb / 10);
	nb %= 10;
	if (nb < 0)
		nb *= -1;
	write(1, &"0123456789"[nb], 1);
}

void	ft_putnbr(int nb)
{
	if (nb < 0)
		write(1, "-", 1);
	ft_putnbr_recur(nb);
}

int	main(int argc, char **argv)
{
	ft_putnbr(atoi(argv[1]));
}
