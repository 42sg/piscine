#include <unistd.h>

int	char_double(int check, char *arr)
{
	int	i;

	i = check - 1;
	while (i >= 0)
	{
		if (arr[i] == arr[check])
			return (1);
		i--;
	}
	return (0);
}

int	contains(char c, char *arr)
{
	while (*arr)
	{
		if (*arr == c)
			return (1);
		arr++;
	}
	return (0);
}

int	main(int argc, char **argv)
{
	int	i;

	i = 0;
	if (argc == 3)
	{
		while (argv[1][i])
		{
			if (!char_double(i, argv[1]))
				write(1, &argv[1][i], 1);
			i++;
		}
		i = 0;
		while (argv[2][i])
		{
			if (!char_double(i, argv[2]) && !contains(argv[2][i], argv[1]))
				write(1, &argv[2][i], 1);
			i++;
		}
	}
	write(1, "\n", 1);
}
