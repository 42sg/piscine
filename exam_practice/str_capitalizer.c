#include <unistd.h>

int	is_space(char c)
{
	return (c == 11 || c == 13 || c == 32);
}

void	put_char(char c)
{
	write(1, &c, 1);
}

void	determine_char(char c, int to_upper)
{
	if (c >= 'a' && c <= 'z' && to_upper)
		put_char(c - 32);
	else if (c >= 'A' && c <= 'Z' && !to_upper)
		put_char(c + 32);
	else
		put_char(c);
}

void	capitalize(char *str)
{
	while (*str)
	{
		while (*str && is_space(*str))
			determine_char(*str++, 0);
		if (!*str)
			break ;
		determine_char(*str++, 1);
		while (*str && !is_space(*str))
			determine_char(*str++, 0);
	}
}

int	main(int argc, char **argv)
{
	if (argc > 1)
	{
		argv++;
		while (*argv)
		{
			capitalize(*argv++);
			write(1, "\n", 1);
		}
		
	}
	else
		write(1, "\n", 1);
}
