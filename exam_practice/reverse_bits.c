/*
The algorithm is basically like atoi, but for base 2
Take the current `val` and multiply by 2 (<< 1)
Then add the next bit to `val`
*/
unsigned char	reverse_bits(unsigned char octet)
{
	int	val;
	int	i;

	i = 0;
	val = 0;
	while (i < 8)
	{
		val = val << 1;
		val += (octet >> i) & 1;
		i++;
	}
	return (val);
}

#include <stdio.h>
int	main(void)
{
	printf("%d", reverse_bits(128));
}
