int	pos_in_base(char c)
{
	int	i;

	i = 0;
	if (c >= 'A' && c <= 'Z')
		c += 32;
	while (i < 16)
	{
		
		if (c == "0123456789abcdef"[i])
			return (i);
		i++;
	}
	return (-1);
}

int	ft_atoi_base(char *c, int str_base)
{
	int	value;
	int	positivity;

	value = 0;
	positivity = 1;
	while (*c == 32 || *c >= 9 && *c <= 13)
		c++;
	if (*c == '-' || *c == '+')
	{
		if (*c == '-')
			positivity = -1;
		c++;
	}
	while ((*c >= '0' && *c <= '9') || (*c >= 'A' && *c <= 'Z') || (*c >= 'a' && *c <= 'z'))
	{
		value *= str_base;
		value += pos_in_base(*c);
		c++;
	}
	return (value * positivity);
}

/*
#include <stdio.h>
int	main(void)
{
	printf("%d", ft_atoi_base("", 2));
}
*/
