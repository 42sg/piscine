int	atoi(char *c)
{
	int	output;
	int	positivity;

	output = 0;
	positivity = 1;
	while ((*c >= 9 && *c <= 13) || *c == ' ')
		c++;
	while (*c == '+' || *c == '-')
	{
		if (*c == '-')
			positivity *= -1;
		c++;
	}
	while (*c >= '0' && *c <= '9')
	{
		output *= 10;
		output += *c - '0';
		c++;
	}
	return (output * positivity);
}
