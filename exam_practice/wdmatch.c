#include <unistd.h>

void	put_str(char *str)
{
	while (*str)
		write(1, str++, 1);
}

int	main(int argc, char **argv)
{
	char	*temp;

	temp = argv[1];
	if (argc == 3)
	{
		while (*argv[1])
		{
			while (*argv[2] && *argv[1] != *argv[2])
				argv[2]++;
			if (*argv[2])
				argv[2]++;
			else
				break;
			argv[1]++;
		}
		if (!*argv[1])
			put_str(temp);
	}
	write(1, "\n", 1);
}
