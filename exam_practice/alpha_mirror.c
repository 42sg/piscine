#include <unistd.h>

void	put_char(char c)
{
	write(1, &c, 1);
}

void	mirror_by_reference(char ref, char to_mirror)
{
	int	diff;

	diff = to_mirror - ref;
	put_char(ref + 25 - diff);
}

void	mirror(char *str)
{
	while (*str)
	{
		if (*str >= 'A' && *str <= 'Z')
			mirror_by_reference('A', *str);
		else if (*str >= 'a' && *str <= 'z')
			mirror_by_reference('a', *str);
		else
			put_char(*str);
		str++;
	}
}

int	main(int argc, char **argv)
{
	if (argc == 2)
		mirror(argv[1]);
	put_char('\n');
}
