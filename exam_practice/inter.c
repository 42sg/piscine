#include <unistd.h>

int	is_double_before(int check, char *arr)
{
	int	i;

	i = check - 1;
	while (i >= 0)
	{
		if (arr[i] == arr[check])
			return (1);
		i--;
	}
	return (0);
}

int	contains(char c, char *arr)
{
	while (*arr)
	{
		if (*arr == c)
			return (1);
		arr++;
	}
	return (0);
}

int	main(int argc, char **argv)
{
	int	i;

	i = 0;
	if (argc == 3)
	{
		while (argv[1][i])
		{
			if (!is_double_before(i, argv[1]))
			{
				if (contains(argv[1][i], argv[2]))
					write(1, &argv[1][i], 1);
			}
			i++;
		}
	}
	write(1, "\n", 1);
	return (0);
}
