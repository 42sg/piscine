#include <unistd.h>

int	power(int base, int exp)
{
	if (exp == 0)
		return (1);
	return (power(base, exp - 1) * base);
}

void	put_int(int i)
{
	char	c;

	c = '0' + i;
	write(1, &c, 1);
}

void	print_bits(unsigned char octet)
{
	int	i;

	i = 7;
	while (i >= 0)
	{
		put_int(octet / power(2, i));
		if (octet >= power(2, i))
			octet -= power(2, i);
		i--;
	}
}

int	main(void)
{
	print_bits(3);
}
