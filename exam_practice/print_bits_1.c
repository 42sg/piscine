#include <unistd.h>
#include "ft_atoi.c"

void	recur(int nb)
{
	if (nb > 1)
		recur(nb / 2);
	nb %= 2;
	write(1, &"01"[nb], 1);
}

int	main(int argc, char **argv)
{
	recur(atoi(argv[1]));
}
