#include <unistd.h>

void	put_char(char c)
{
	write(1, &c, 1);
}

int	main(int argc, char **argv)
{
	if (argc == 2)
	{
		while (*argv[1])
		{
			if (*argv[1] >= 'A' && *argv[1] <= 'Z')
				put_char((*argv[1] - 'A' + 13) % 26 + 'A');
			else if (*argv[1] >= 'a' && *argv[1] <= 'z')
				put_char((*argv[1] -'a' + 13) % 26 + 'a');
			else
				put_char(*argv[1]);
			argv[1]++;
		}
	}
	write(1, "\n", 1);
}
