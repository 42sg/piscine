#include <unistd.h>

void	put_char(char c)
{
	int	i;

	i = 0;
	if (c >= 'A' && c <= 'Z')
	{
		while (i < c - 'A' + 1)
		{
			write(1, &c, 1);
			i++;
		}
	}
	else if (c >= 'a' && c <= 'z')
	{
		while (i < c - 'a' + 1)
		{
			write(1, &c, 1);
			i++;
		}
	}
	else
		write(1, &c, 1);
}

int	main(int argc, char **argv)
{
	if (argc == 2)
	{
		while (*argv[1])
			put_char(*argv[1]++);
	}
	return (0);
}
