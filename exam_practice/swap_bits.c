unsigned char	swap_bits(unsigned char octet)
{
	unsigned char	val;

	val = octet & 15;
	val = val << 4;
	val += octet >> 4;
	return (val);
}

#include <stdio.h>
int	main(void)
{
	printf("%d\n", swap_bits('A'));
}
