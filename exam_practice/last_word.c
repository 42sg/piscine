#include <unistd.h>

int	is_space(char c)
{
	return (c == 11 || c == 13 || c == 32);
}

int	main(int argc, char **argv)
{
	int	start;
	int	end;

	end = 0;
	if (argc == 2)
	{
		while (argv[1][end])
			end++;
		if (end != 0)
		{		
			end--;
			while (argv[1][end] && is_space(argv[1][end]))
				end--;
			start = end;
			while (argv[1][start] && !is_space(argv[1][start]))
				start--;
			start++;
			while (start <= end)
				write(1, &argv[1][start++], 1);
		}
	}
	write(1, "\n", 1);
	return (0);
}
