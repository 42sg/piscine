#include <unistd.h>
#include "ft_atoi.c"

int	len(char *str)
{
	int	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

void	recur(int nb, char *base_charset)
{
	if (nb > len(base_charset) - 1 || nb < -len(base_charset) + 1)
		recur(nb / len(base_charset), base_charset);
	nb %= len(base_charset);
	if (nb < 0)
		nb *= -1;
	write(1, &base_charset[nb], 1);
}

/*
argv[1] number in integer
argv[2] base charset

e.g. 10 "0123456789abcdef"
means print integer 10 in base 16

this example assumes the base charset is valid and does not check for it
*/
int	main(int argc, char **argv)
{
	int	val;

	if (argc == 3)
	{
		val = atoi(argv[1]);
		if (val < 0)
			write(1, "-", 1);
		recur(val, argv[2]);
	}
}
