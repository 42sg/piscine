int	is_power_of_2(unsigned int n)
{
	if (n < 0)
		return (0);
	if (n == 1)
		return (1);
	if (n % 2 != 0)
		return (0);
	return (is_power_of_2(n / 2));
}

#include <stdio.h>
int	main(void)
{
	printf("%d\n", is_power_of_2(2));
	printf("%d\n", is_power_of_2(4));
	printf("%d\n", is_power_of_2(6));
	printf("%d\n", is_power_of_2(8));
	printf("%d\n", is_power_of_2(10));
	printf("%d\n", is_power_of_2(12));
}
