#include <unistd.h>
#include "ft_atoi.c"

int	atoi(char *c);

int	is_prime(int nb)
{
	int	i;

	i = 2;
	if (nb < 2)
		return (0);
	if (nb == 2 || nb == 3)
		return (1);
	while (i <= nb / i)
	{
		if (nb % i == 0)
			return (0);
		i++;
	}
	return (1);
}

void	put_nbr(int i)
{
	if (i > 9 || i < -9)
		put_nbr(i / 10);
	i %= 10;
	write(1, &"0123456789"[i], 1);
}

int	main(int argc, char **argv)
{
	int	i;
	int	value;
	int	count;

	i = 0;
	count = 0;
	if (argc == 2)
	{
		value = atoi(argv[1]);
		if (value > 0)
		{
			while (i <= value)
			{
				if (is_prime(i))
					count += i;
				i++;
			}
		}
	}
	put_nbr(count);
	write(1, "\n", 1);
}
