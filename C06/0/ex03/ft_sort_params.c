/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/30 15:21:24 by keyu              #+#    #+#             */
/*   Updated: 2023/06/30 16:05:28 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int	cmp(char *a, char *b)
{
	while (*a && *b && *a == *b)
	{
		a++;
		b++;
	}
	return (*a - *b);
}

void	bubble_sort(int argc, char **argv)
{
	int		i;
	int		j;
	char	*temp_ptr;

	i = 1;
	while (i < argc - 1)
	{
		j = 1;
		while (j < argc - i)
		{
			if (cmp(argv[j], argv[j + 1]) > 0)
			{
				temp_ptr = argv[j + 1];
				argv[j + 1] = argv[j];
				argv[j] = temp_ptr;
			}
			j++;
		}
		i++;
	}
}

int	main(int argc, char **argv)
{
	int	index;

	if (argc > 2)
		bubble_sort(argc, argv);
	index = 1;
	while (index < argc)
	{
		while (*argv[index])
			write(1, argv[index]++, 1);
		write(1, "\n", 1);
		index++;
	}
	return (0);
}
