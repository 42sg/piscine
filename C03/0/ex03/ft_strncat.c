/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/26 15:33:23 by keyu              #+#    #+#             */
/*   Updated: 2023/06/26 15:44:16 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strncat(char *dest, char *src, unsigned int nb)
{
	unsigned int	dest_count;
	unsigned int	index;

	dest_count = 0;
	while (dest[dest_count])
		dest_count++;
	index = 0;
	while (src[index] && index < nb)
	{
		dest[dest_count + index] = src[index];
		index++;
	}
	dest[dest_count + index] = 0;
	return (dest);
}
