/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/26 15:23:22 by keyu              #+#    #+#             */
/*   Updated: 2023/06/26 15:32:23 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcat(char *dest, char *src)
{
	int	index;
	int	dest_count;

	dest_count = 0;
	while (dest[dest_count])
		dest_count++;
	index = 0;
	while (src[index])
	{
		dest[dest_count + index] = src[index];
		index++;
	}
	dest[dest_count + index] = 0;
	return (dest);
}
