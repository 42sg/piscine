/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/26 15:44:56 by keyu              #+#    #+#             */
/*   Updated: 2023/06/26 16:04:30 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strstr(char *str, char *to_find)
{
	int	to_find_length;
	int	str_length;
	int	index;
	int	sub_i;

	to_find_length = 0;
	str_length = 0;
	index = 0;
	while (to_find[to_find_length])
		to_find_length++;
	while (str[str_length])
		str_length++;
	if (to_find_length > str_length)
		return (0);
	while (index < str_length - to_find_length + 1)
	{
		sub_i = 0;
		while (str[index + sub_i] == to_find[sub_i] && sub_i < to_find_length)
			sub_i++;
		if (sub_i == to_find_length)
			return (str + index);
		index++;
	}
	return (0);
}
