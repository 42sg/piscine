/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_int_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/22 16:55:10 by keyu              #+#    #+#             */
/*   Updated: 2023/06/22 17:13:52 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	swap(int *arr, int index_a, int index_b)
{
	int	temp;

	temp = arr[index_a];
	arr[index_a] = arr[index_b];
	arr[index_b] = temp;
}

void	ft_sort_int_tab(int *tab, int size)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (i < size)
	{
		while (j < size - i - 1)
		{
			if (tab[j] > tab[j + 1])
				swap(tab, j, j + 1);
			j++;
		}
		j = 0;
		i++;
	}
}
