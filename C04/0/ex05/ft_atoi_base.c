/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/28 17:04:32 by keyu              #+#    #+#             */
/*   Updated: 2023/06/28 17:32:06 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	len(char *c)
{
	int	index;

	index = 0;
	while (c[index])
		index++;
	return (index);
}

int	all_distinct_chars(char *c)
{
	int	i;
	int	j;
	int	length;

	i = 0;
	length = len(c);
	while (i < length - 1)
	{
		j = i + 1;
		while (j < length)
		{
			if (c[j] == c[i])
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

int	base_is_valid(char *base)
{
	int	index;

	index = 0;
	while (base[index])
	{
		if (base[index] == '+' || base[index] == '-' || base[index] == ' ')
			return (0);
		index++;
	}
	return (index > 1 && all_distinct_chars(base));
}

int	position_in_base(char c, char *base)
{
	int	index;

	index = 0;
	while (base[index])
	{
		if (base[index] == c)
			return (index);
		index++;
	}
	return (-1);
}

int	ft_atoi_base(char *str, char *base)
{
	int	index;
	int	output;
	int	positivity;

	index = 0;
	output = 0;
	positivity = 1;
	if (!base_is_valid(base))
		return (0);
	while (str[index])
	{
		if (str[index] == '-')
			positivity *= -1;
		if (str[index] == ' ' || str[index] == '+' || str[index] == '-')
		{
			index++;
			continue ;
		}
		if (position_in_base(str[index], base) == -1)
			break ;
		output *= len(base);
		output += position_in_base(str[index], base);
		index++;
	}
	return (output * positivity);
}
