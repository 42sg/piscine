/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/26 18:39:22 by keyu              #+#    #+#             */
/*   Updated: 2023/06/28 14:55:07 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	char_is_valid(char c)
{
	return ((c >= '0' && c <= '9') || c == '-' || c == '+');
}

int	ft_atoi(char *str)
{
	int	output;
	int	positivity;
	int	number_started;

	output = 0;
	positivity = 1;
	number_started = 0;
	while (*str)
	{
		if (!char_is_valid(*str) && *str != ' ')
			break ;
		if (char_is_valid(*str))
			number_started = 1;
		if (number_started && !char_is_valid(*str))
			break ;
		if (*str == '-')
			positivity *= -1;
		if (*str >= '0' && *str <= '9')
		{
			output *= 10;
			output += *str - '0';
		}
		str++;
	}
	return (output * positivity);
}
