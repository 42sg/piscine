/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/28 14:56:46 by keyu              #+#    #+#             */
/*   Updated: 2023/06/28 17:02:03 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	len(char *c)
{
	int	index;

	index = 0;
	while (c[index])
		index++;
	return (index);
}

int	all_distinct_chars(char *c)
{
	int	i;
	int	j;
	int	length;

	i = 0;
	length = len(c);
	while (i < length - 1)
	{
		j = i + 1;
		while (j < length)
		{
			if (c[j] == c[i])
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

int	base_is_valid(char *base)
{
	int	index;

	index = 0;
	while (base[index])
	{
		if (base[index] == '+' || base[index] == '-')
			return (0);
		index++;
	}
	return (index > 1 && all_distinct_chars(base));
}

void	recursive_write(long nbr, char *base)
{
	int	length;

	if (!base_is_valid(base))
		return ;
	length = len(base);
	if (nbr < 0)
	{
		write(1, "-", 1);
		nbr *= -1;
	}
	if (nbr >= length)
	{
		recursive_write(nbr / length, base);
	}
	write(1, &base[nbr % length], 1);
}

void	ft_putnbr_base(int nbr, char *base)
{
	recursive_write((long) nbr, base);
}
