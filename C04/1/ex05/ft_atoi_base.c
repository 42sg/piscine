/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/28 17:04:32 by keyu              #+#    #+#             */
/*   Updated: 2023/07/07 15:38:15 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	len(char *c)
{
	int	index;

	index = 0;
	while (c[index])
		index++;
	return (index);
}

int	all_distinct_chars(char *c)
{
	int	i;
	int	j;
	int	length;

	i = 0;
	length = len(c);
	while (i < length - 1)
	{
		j = i + 1;
		while (j < length)
		{
			if (c[j] == c[i])
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

int	base_is_valid(char *base)
{
	if (len(base) < 2)
		return (0);
	while (*base)
	{
		if (*base == '+' || *base == '-')
			return (0);
		if (*base == ' ' || (*base >= 9 && *base <= 13))
			return (0);
		base++;
	}
	return (all_distinct_chars(base));
}

int	position_in_base(char c, char *base)
{
	int	index;

	index = 0;
	while (base[index])
	{
		if (base[index] == c)
			return (index);
		index++;
	}
	return (-1);
}

int	ft_atoi_base(char *str, char *base)
{
	int	output;
	int	positivity;

	output = 0;
	positivity = 1;
	if (!base_is_valid(base))
		return (0);
	while (*str == ' ' || (*str >= 9 && *str <= 13))
		str++;
	while (*str == '+' || *str == '-')
	{
		if (*str == '-')
			positivity *= -1;
		str++;
	}
	while (*str)
	{
		if (position_in_base(*str, base) == -1)
			break ;
		output *= len(base);
		output += position_in_base(*str, base);
		str++;
	}
	return (output * positivity);
}
