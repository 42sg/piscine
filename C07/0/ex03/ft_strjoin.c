/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/30 12:18:02 by keyu              #+#    #+#             */
/*   Updated: 2023/06/30 13:56:11 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int	len_str(char *str)
{
	int	index;

	index = 0;
	while (str[index])
		index++;
	return (index);
}

void	add(char *from, char *to, int start_from)
{
	int	i;

	i = 0;
	while (i < len_str(from))
	{
		to[start_from + i] = from[i];
		i++;
	}
}

char	*allocate_memory(int size, char **strs, char *sep)
{
	int	index;
	int	total_size;

	index = 0;
	total_size = 0;
	while (index < size)
	{
		total_size += len_str(strs[index]);
		index++;
	}
	total_size += len_str(sep) * (size - 1) + 1;
	return (malloc(total_size * sizeof(char)));
}

char	*ft_strjoin(int size, char **strs, char *sep)
{
	int		index;
	int		counter;
	char	*output;

	if (size == 0)
		return (malloc(sizeof(char)));
	index = 0;
	counter = 0;
	output = allocate_memory(size, strs, sep);
	while (index < size)
	{
		add(strs[index], output, counter);
		counter += len_str(strs[index]);
		if (len_str(sep) > 0 && index < size - 1)
		{
			add(sep, output, counter);
			counter += len_str(sep);
		}
		index++;
	}
	output[counter] = 0;
	return (output);
}
