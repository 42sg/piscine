/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/30 11:21:42 by keyu              #+#    #+#             */
/*   Updated: 2023/06/30 11:33:23 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	*ft_range(int min, int max)
{
	int	index;
	int	*output;

	index = 0;
	if (min >= max)
		return (0);
	output = malloc((max - min) * sizeof(int));
	while (index < max - min)
	{
		output[index] = min + index;
		index++;
	}
	return (output);
}
