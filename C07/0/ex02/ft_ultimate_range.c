/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/30 11:35:27 by keyu              #+#    #+#             */
/*   Updated: 2023/07/03 18:06:48 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	ft_ultimate_range(int **range, int min, int max)
{
	int	index;
	int	*arr;

	if (min >= max)
	{
		*range = 0;
		return (0);
	}
	arr = malloc((max - min) * sizeof(int));
	if (arr == 0)
		return (-1);
	index = 0;
	while (index < max - min)
	{
		arr[index] = min + index;
		index++;
	}
	range = &arr;
	return (max - min);
}

int main(){
	int *range;

	ft_ultimate_range(&range, 2, 5);
}
