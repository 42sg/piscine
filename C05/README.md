C05

| Try | Result |
| --- | ------ |
| 0   | ex00: OK, ex01: OK, ex02: KO, ex03: KO, ex04: OK, ex05: Timeout, KO, ex06: OK, ex07: Timeout, KO, ex08: Nothing turned in |
| 1   | ex00: OK, ex01: OK, ex02: OK, ex03: OK, ex04: OK, ex05: OK, ex06: OK, ex07: OK, ex08: Nothing turned in |
