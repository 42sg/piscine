/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/22 13:00:25 by keyu              #+#    #+#             */
/*   Updated: 2023/06/22 13:17:54 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	write_single_digit(int i)
{
	char	result;

	result = '0' + i;
	write(1, &result, 1);
}

void	ft_putnbr(int nb)
{
	int	mod10;
	int	remainder;

	if (nb < 0)
	{
		write(1, "-", 1);
		nb *= -1;
	}
	mod10 = nb / 10;
	remainder = nb % 10;
	if (mod10 != 0)
		ft_putnbr(mod10);
	write_single_digit(remainder);
}
