/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/22 11:34:58 by keyu              #+#    #+#             */
/*   Updated: 2023/06/22 12:07:48 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	write_chars(char a, char b, char c, char d)
{
	write(1, &a, 1);
	write(1, &b, 1);
	write(1, " ", 1);
	write(1, &c, 1);
	write(1, &d, 1);
	if (a != '9' || b != '8' || c != '9' || c != '9')
		write(1, ", ", 2);
}

void	write_from_to_max(char a, char b, char c, char d)
{
	while (c <= '9')
	{
		while (d <= '9')
		{
			write_chars(a, b, c, d);
			d++;
		}
		c++;
		d = '0';
	}
}

void	ft_print_comb2(void)
{
	char	a1;
	char	a2;

	a1 = '0';
	a2 = '0';
	while (a1 <= '9')
	{
		while (a2 <= '9')
		{
			write_from_to_max(a1, a2, a1, a2 + 1);
			a2++;
		}
		a1++;
		a2 = '0';
	}
}
