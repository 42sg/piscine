### Highlighting some logic that I think are smart that I've gathered so far

#### 1. Checking if a number if prime by checking conditions up till the sqrt of that number
Typical:
```
if (i * i <= number)
```
However, this will error especially if `i` is an `int` and `number` is an `int`. i = 46340 is ok, i = 46341 onwards will overflow.

Smart:
```
if (i <= number / i)
```
It's the same thing except that we divide the `i` on both sides...

#### 2. putnbr - writing out chars from `int`
Typical:
```
if (nb == -2147483648)
	write(1, "-2147483648", 11);
```

Another typical:
```
long	temp;

temp = (long)nb;
```

All this to avoid overflow in handling super negative when later on it gets multiplied by -1.

An interesting method (though it takes two functions):
```
void	recur(int nb)
{
	if (nb > 9 || nb < -9)
		recur(nb / 10);
	nb %= 10;
	if (nb < 0)
		nb *= -1;
	write(1, &"0123456789"[nb], 1);
}

void	putnbr(int nb)
{
	if (nb < 0)
		write(1, "-", 1);
	recur(nb);
}
```

The above method is a general solution to print in any base.

Refer to:
- [base 10 / decimal](../exam_practice/ft_putnbr.c)
- [base 16 / hex](../exam_practice/print_hex.c)
- [base 2 / binary](../exam_practice/print_bits_1.c)
- [base N](../exam_practice/ft_putnbr_base.c)
