### Prerequisite
- compile with symbols using `-g` flag
- run using `gdb [binary]` where binary is the compiled file

### Display
- layout next (might show assembly code though)
- layout src (show source code)
- refresh
- quit or q

### Breakpoints
- break [...] or b [...]
- delete [...]
- clear

### Debugging
- run [...args] or r [...args]
- next or n (in current scope)
- step (into function)
- up (out of function)
- backtrace (to show trace where program failed)

### Reading
- print [...] or p [...]
  - print a variable
  - for pointer, use `*ptr`
  - to show contents of first N variables ahead of ptr, `*ptr@N`
  - it can return the value of a function as well
- info locals (to print all variables in current scope)

### Others
- Enter to repeat previous command
