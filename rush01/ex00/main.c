/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/01 12:45:22 by keyu              #+#    #+#             */
/*   Updated: 2023/07/01 17:25:11 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

void	write_chars(char *arr);
int		len(char *arr);
int		parse_iterative(char *from, int *to);
void	solve(int *puzzle, int *clues);

int	*create_blank(void)
{
	return (malloc(16 * sizeof(int)));
}

int	main(int argc, char **argv)
{
	int	*numbers;

	numbers = malloc(16 * sizeof(int));
	if (argc != 2 || len(argv[1]) != 31 || !parse_iterative(argv[1], numbers))
	{
		write_chars("Error\n");
		return (1);
	}
	solve(create_blank(), numbers);
	return (0);
}
