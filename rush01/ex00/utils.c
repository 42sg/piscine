/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/01 12:45:22 by keyu              #+#    #+#             */
/*   Updated: 2023/07/01 17:25:11 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	write_chars(char *arr)
{
	while (*arr)
		write(1, arr++, 1);
}

void	put_char(char c)
{
	write(1, &c, 1);
}

void	put_int(int i)
{
	put_char('0' + i);
}

int	len(char *arr)
{
	int	i;

	i = 0;
	while (arr[i])
		i++;
	return (i);
}

void	write_matrix(int *arr)
{
	int	i;

	i = 0;
	while (i < 4)
	{
		put_int(arr[i * 4 + 0]);
		put_char(' ');
		put_int(arr[i * 4 + 1]);
		put_char(' ');
		put_int(arr[i * 4 + 2]);
		put_char(' ');
		put_int(arr[i * 4 + 3]);
		i++;
		write(1, "\n", 1);
	}
}
