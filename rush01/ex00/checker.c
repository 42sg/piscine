/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/01 12:45:22 by keyu              #+#    #+#             */
/*   Updated: 2023/07/01 17:25:11 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	check_row_legal(int clue, int *arr, int row_num, int to_reverse)
{
	int	i;
	int	count;
	int	current_max;

	if (to_reverse)
		i = 3;
	else
		i = 0;
	count = 0;
	current_max = 0;
	while (i < 4 && i >= 0)
	{
		if (arr[row_num * 4 + i] > current_max)
		{
			current_max = arr[row_num * 4 + i];
			count++;
		}
		if (to_reverse)
			i--;
		else
			i++;
	}
	return (count == clue);
}

int	check_col_legal(int clue, int *arr, int col_num, int to_reverse)
{
	int	i;
	int	count;
	int	current_max;

	if (to_reverse)
		i = 3;
	else
		i = 0;
	count = 0;
	current_max = 0;
	while (i < 4 && i >= 0)
	{
		if (arr[i * 4 + col_num] > current_max)
		{
			current_max = arr[i * 4 + col_num];
			count++;
		}
		if (to_reverse)
			i--;
		else
			i++;
	}
	return (count == clue);
}

int	check_double(int *puzzle, int row, int col)
{
	int	index;

	index = 0;
	while (index < 4)
	{
		if (col != index && puzzle[row * 4 + index] == puzzle[row * 4 + col])
			return (1);
		index++;
	}
	index = 0;
	while (index < 4)
	{
		if (row != index && puzzle[index * 4 + col] == puzzle[row * 4 + col])
			return (1);
		index++;
	}
	return (0);
}

int	row_col_legal(int *puzzle, int *clues)
{
	int	i;

	i = 0;
	while (i < 16)
	{
		if (i < 4 && !check_col_legal(clues[i], puzzle, i, 0))
			return (0);
		if (i >= 4 && i < 8 && !check_col_legal(clues[i], puzzle, i - 4, 1))
			return (0);
		if (i >= 8 && i < 12 && !check_row_legal(clues[i], puzzle, i - 8, 0))
			return (0);
		if (i >= 12 && i < 16 && !check_row_legal(clues[i], puzzle, i - 12, 1))
			return (0);
		i++;
	}
	return (1);
}
