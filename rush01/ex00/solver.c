/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solver.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/01 12:45:22 by keyu              #+#    #+#             */
/*   Updated: 2023/07/01 17:25:11 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	write_chars(char *arr);
void	write_matrix(int *arr);
int		row_col_legal(int *puzzle, int *clues);
int		check_double(int *puzzle, int row, int col);

char	*permutation_set_a(int i)
{
	if (i == 0)
		return ("1234");
	else if (i == 1)
		return ("1243");
	else if (i == 2)
		return ("1324");
	else if (i == 3)
		return ("1342");
	else if (i == 4)
		return ("1423");
	else if (i == 5)
		return ("1432");
	else if (i == 6)
		return ("2134");
	else if (i == 7)
		return ("2143");
	else if (i == 8)
		return ("2314");
	else if (i == 9)
		return ("2341");
	else if (i == 10)
		return ("2413");
	else if (i == 11)
		return ("2431");
	return ("");
}

char	*permutation_set_b(int i)
{
	if (i == 12)
		return ("3124");
	else if (i == 13)
		return ("3142");
	else if (i == 14)
		return ("3214");
	else if (i == 15)
		return ("3241");
	else if (i == 16)
		return ("3412");
	else if (i == 17)
		return ("3421");
	else if (i == 18)
		return ("4123");
	else if (i == 19)
		return ("4132");
	else if (i == 20)
		return ("4213");
	else if (i == 21)
		return ("4231");
	else if (i == 22)
		return ("4312");
	else if (i == 23)
		return ("4321");
	return ("");
}

void	permutate_for_row(int *puzzle, int row, int perm)
{
	int		i;
	char	*perms;

	i = 0;
	if (perm < 12)
		perms = permutation_set_a(perm);
	else
		perms = permutation_set_b(perm);
	while (i < 4)
	{
		puzzle[row * 4 + i] = perms[i] - '0';
		i++;
	}
}

int	is_valid_solution(int *puzzle, int *clues)
{
	int	i;
	int	j;

	if (!row_col_legal(puzzle, clues))
		return (0);
	i = 0;
	j = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			if (check_double(puzzle, i, j))
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

void	solve(int *puzzle, int *clues)
{
	int	perm;

	perm = 0;
	while (perm < 331776)
	{
		permutate_for_row(puzzle, 0, (perm / 13824) % 24);
		permutate_for_row(puzzle, 1, (perm / 576) % 24);
		permutate_for_row(puzzle, 2, (perm / 24) % 24);
		permutate_for_row(puzzle, 3, perm % 24);
		if (is_valid_solution(puzzle, clues))
		{
			write_matrix(puzzle);
			return ;
		}
		perm++;
	}
	write_chars("Error\n");
}
