/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush04.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: the <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/24 18:56:18 by the               #+#    #+#             */
/*   Updated: 2023/06/24 18:56:43 by the              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	line(int x, char start, char middle, char end)
{
	int	xcount;

	xcount = 0;
	if (x >= 1)
		ft_putchar(start);
	while (xcount < x - 2)
	{
		ft_putchar(middle);
		xcount++;
	}
	if (x >= 2)
		ft_putchar(end);
	ft_putchar('\n');
}

void	rush(int x, int y)
{
	int	ycount;

	ycount = 0;
	if (y >= 1)
		line(x, 'A', 'B', 'C');
	while (ycount < y - 2)
	{
		line(x, 'B', ' ', 'B');
		ycount++;
	}
	if (y >= 2)
		line(x, 'C', 'B', 'A');
}
