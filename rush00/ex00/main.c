/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: the <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/24 16:26:42 by the               #+#    #+#             */
/*   Updated: 2023/06/24 18:47:12 by the              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	rush(int x, int y);

int	atoi(char *str)
{
	int	output;
	int	index;
	int	positivity;

	output = 0;
	index = 0;
	positivity = 1;
	while (str[index] != '\0')
	{
		if (str[index] == '-')
		{
			positivity = -1;
		}
		else
		{
			output *= 10;
			output += str[index] - '0';
		}
		index++;
	}
	return (output * positivity);
}

int	main(int argc, char **argv)
{
	int	x;
	int	y;

	if (argc != 3)
	{
		return (0);
	}
	x = atoi(argv[1]);
	y = atoi(argv[2]);
	if (x < 0 || y < 0)
	{
		return (0);
	}
	rush(x, y);
}
