/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_printable.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/23 12:11:44 by keyu              #+#    #+#             */
/*   Updated: 2023/06/26 12:56:06 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	char_is_printable(char c)
{
	return (c >= ' ' && c <= '~');
}

int	ft_str_is_printable(char *str)
{
	int	index;

	index = 0;
	while (str[index] != '\0')
	{
		if (!char_is_printable(str[index]))
		{
			return (0);
		}
		index++;
	}
	return (1);
}
