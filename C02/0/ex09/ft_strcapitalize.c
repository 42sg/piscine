/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/23 12:56:45 by keyu              #+#    #+#             */
/*   Updated: 2023/06/24 10:05:54 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	char_is_lowercase(char c)
{
	return (c >= 'a' && c <= 'z');
}

int	char_is_uppercase(char c)
{
	return (c >= 'A' && c <= 'Z');
}

int	char_is_alphabet(char c)
{
	return (char_is_uppercase(c) || char_is_lowercase(c));
}

int	char_is_word_character(char c)
{
	return (char_is_alphabet(c) || (c >= '0' && c <= '9'));
}

char	*ft_strcapitalize(char *str)
{
	int	index;
	int	currently_is_word;

	index = 0;
	currently_is_word = 0;
	while (str[index] != '\0')
	{
		if (currently_is_word && char_is_uppercase(str[index]))
			str[index] += 32;
		if (!currently_is_word && char_is_word_character(str[index]))
		{
			currently_is_word = 1;
			if (char_is_lowercase(str[index]))
				str[index] -= 32;
		}
		currently_is_word = char_is_word_character(str[index]);
		index++;
	}
	return (str);
}
