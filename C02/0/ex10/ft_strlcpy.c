/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/26 12:14:20 by keyu              #+#    #+#             */
/*   Updated: 2023/06/26 12:58:45 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size)
{
	unsigned int	index;

	index = 0;
	while (index < size - 1 && src[index] != 0)
	{
		dest[index] = src[index];
		index++;
	}
	dest[index] = 0;
	index = 0;
	while (src[index])
	{
		index++;
	}
	return (index);
}
