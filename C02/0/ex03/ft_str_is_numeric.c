/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_numeric.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/23 11:45:44 by keyu              #+#    #+#             */
/*   Updated: 2023/06/23 11:55:40 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	char_is_numeric(char c)
{
	return (c >= '0' && c <= '9');
}

int	ft_str_is_numeric(char *str)
{
	int	index;

	index = 0;
	while (str[index] != '\0')
	{
		if (!char_is_numeric(str[index]))
			return (0);
		index++;
	}
	return (1);
}
