/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_non_printable.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/26 12:27:22 by keyu              #+#    #+#             */
/*   Updated: 2023/06/26 13:16:53 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int	is_printable(char c)
{
	return (c >= ' ' && c <= '~');
}

void	convert_int_to_hexstr(int i)
{
	char	output;

	if (i >= 0 && i <= 9)
		output = '0' + i;
	else
		output = 'a' + i - 10;
	write(1, &output, 1);
}

void	write_hex(char c)
{
	int	divisor;
	int	remainder;

	remainder = c % 16;
	divisor = c / 16;
	convert_int_to_hexstr(divisor);
	convert_int_to_hexstr(remainder);
}

void	ft_putstr_non_printable(char *str)
{
	int	index;

	index = 0;
	while (str[index])
	{
		if (is_printable(str[index]))
		{		
			write(1, &str[index], 1);
		}
		else
		{
			write(1, "\\", 1);
			write_hex(str[index]);
		}
		index++;
	}
}
