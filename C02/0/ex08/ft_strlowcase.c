/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlowcase.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/23 12:28:53 by keyu              #+#    #+#             */
/*   Updated: 2023/06/23 12:33:35 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	char_is_uppercase(char c)
{
	return (c >= 'A' && c <= 'Z');
}

char	*ft_strlowcase(char *str)
{
	int	index;

	index = 0;
	while (str[index] != '\0')
	{
		if (char_is_uppercase(str[index]))
		{
			str[index] += 32;
		}
		index++;
	}
	return (str);
}
