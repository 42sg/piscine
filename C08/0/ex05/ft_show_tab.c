/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_show_tab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/06 11:42:52 by keyu              #+#    #+#             */
/*   Updated: 2023/07/06 11:45:23 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "ft_stock_str.h"

void	write_str(char *str)
{
	while (*str)
		write(1, str++, 1);
	write(1, "\n", 1);
}

void	write_char(char c)
{
	write(1, &c, 1);
}

void	put_nbr(int i)
{
	if (i > 9)
		put_nbr(i / 10);
	write_char((i % 10) + '0');
}

void	ft_show_tab(struct s_stock_str *par)
{
	int	i;

	i = 0;
	while (par[i].str != 0)
	{
		write_str(par[i].str);
		put_nbr(par[i].size);
		write(1, "\n", 1);
		write_str(par[i].copy);
		i++;
	}
}
