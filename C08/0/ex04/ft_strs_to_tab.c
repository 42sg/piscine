/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strs_to_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <keyu@student.42singapore.sg>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/06 09:45:51 by keyu              #+#    #+#             */
/*   Updated: 2023/07/06 11:45:01 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_stock_str.h"

void	str_dup(char *from, char *to)
{
	while (*from)
		*to++ = *from++;
	*to = 0;
}

int	len(char *arr)
{
	int	i;

	i = 0;
	while (arr[i])
		i++;
	return (i);
}

struct	s_stock_str	*ft_strs_to_tab(int ac, char **av)
{
	int			i;
	t_stock_str	*output;
	char		*temp_malloc;

	output = malloc((ac + 1) * sizeof(t_stock_str));
	if (output == 0)
		return (0);
	i = 0;
	while (i < ac)
	{
		temp_malloc = (char *)malloc((len(av[i]) + 1) * sizeof(char));
		if (temp_malloc == 0)
			return (0);
		output[i].size = len(av[i]);
		output[i].str = av[i];
		str_dup(av[i], temp_malloc);
		output[i].copy = temp_malloc;
		i++;
	}
	output[i] = (struct s_stock_str){0, 0, 0};
	return (output);
}
