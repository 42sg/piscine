/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   itoarr.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wiltan <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/08 11:44:11 by wiltan            #+#    #+#             */
/*   Updated: 2023/07/08 12:27:29 by wiltan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush02.h"

int	intlen(unsigned int nb)
{
	int	i;

	i = 0;
	while (nb > 0)
	{
		nb /= 1000;
		i++;
	}
	return (i);
}

int	*ft_itoarr(unsigned int nb)
{
	int	*arr;
	int	j;

	j = 0;
	arr = (int *)malloc(intlen(nb) * sizeof(int));
	if (!arr)
		return (NULL);
	while (nb > 0)
	{
		arr[j] = (nb % 1000);
		nb /= 1000;
		j++;
	}
	return (arr);
}
