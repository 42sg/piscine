/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush02.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wiltan <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/09 11:20:42 by wiltan            #+#    #+#             */
/*   Updated: 2023/07/09 11:36:52 by wiltan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RUSH02_H
# define RUSH02_H

# include <stdlib.h>
# include <unistd.h>
# include <stdio.h>
# include <fcntl.h>

typedef struct s_parsed {
	char	**rows;
	int		rc;
}	t_parsed;

int		triple(int nb, t_parsed parsed);
int		put_value(char *key, char **rows, int row_count, int to_space);
void	rush(char *dict, unsigned int value);
void	put_str(char *str);
void	duplicate_into_rows(char *file_contents, char **rows);
int		itostr_write(int nb, char **rows, int row_count, int to_space);
char	**allocate_row(char *buf);
char	*get_file_contents(char *dict);
char	**allocate_row(char *buf);
char	*get_file_contents(char *dict);
int		row_count(char *buf);
int		*ft_itoarr(unsigned int nb);
int		intlen(unsigned int nb);
long	custom_atol(char *arr);

#endif
