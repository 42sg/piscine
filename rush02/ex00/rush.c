/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/08 12:40:06 by keyu              #+#    #+#             */
/*   Updated: 2023/07/08 19:27:36 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush02.h"

t_parsed	dict_rows(char *dict, unsigned int value)
{
	char	**rows;
	char	*file_contents;
	int		rc;

	if (dict == 0)
		file_contents = get_file_contents("numbers.dict");
	else
		file_contents = get_file_contents(dict);
	rows = allocate_row(file_contents);
	rc = row_count(file_contents);
	duplicate_into_rows(file_contents, rows);
	if (value == 0)
		put_value("0", rows, rc, 0);
	free(file_contents);
	return ((struct s_parsed){rows, rc});
}

int	requires_space(int *hundreds_arr, int curr)
{
	int	i;

	i = curr - 1;
	while (i >= 0)
	{
		if (hundreds_arr[i] > 0)
			return (1);
		i--;
	}
	return (0);
}

void	write_places(unsigned int nb, t_parsed parsed)
{
	char	*key;
	int		length;
	int		i;

	length = nb * 3;
	key = malloc((length + 2) * sizeof(char));
	key[0] = '1';
	i = 1;
	while (i < length + 1)
		key[i++] = '0';
	key[i] = 0;
	put_value(key, parsed.rows, parsed.rc, 0);
	free(key);
}

void	ft_free(t_parsed parsed, int *hundreds_arr)
{
	int	i;

	i = 0;
	while (i < parsed.rc)
		free(parsed.rows[i++]);
	free(parsed.rows);
	free(hundreds_arr);
}

void	rush(char *dict, unsigned int value)
{
	int			*hundreds_arr;
	t_parsed	parsed;
	int			i;

	hundreds_arr = ft_itoarr(value);
	parsed = dict_rows(dict, value);
	i = intlen(value) - 1;
	while (i >= 0)
	{
		if (hundreds_arr[i] != 0)
		{
			if (!triple(hundreds_arr[i], parsed))
				break ;
			if (i != 0)
			{
				put_str(" ");
				write_places(i, parsed);
				if (requires_space(hundreds_arr, i))
					put_str(", ");
			}
		}
		i--;
	}
	ft_free(parsed, hundreds_arr);
}
