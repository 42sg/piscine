/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   itostr.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wiltan <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/08 14:13:10 by wiltan            #+#    #+#             */
/*   Updated: 2023/07/08 19:44:47 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush02.h"

void	set_hundred(char *str)
{
	str[0] = '1';
	str[1] = '0';
	str[2] = '0';
	str[3] = 0;
}

char	*itostr(int nb)
{
	char	*str;

	str = (char *)malloc(4 * sizeof(char));
	if (nb >= 100)
		set_hundred(str);
	else if (nb >= 20 && nb < 100)
	{
		str[0] = nb / 10 + '0';
		str[1] = '0';
		str[2] = 0;
	}
	else if (nb >= 10 && nb < 20)
	{
		str[0] = nb / 10 + '0';
		str[1] = nb - (nb / 10) * 10 + '0';
		str[2] = 0;
	}
	else if (nb >= 1 && nb < 10)
	{
		str[0] = nb + '0';
		str[1] = 0;
	}
	return (str);
}

int	itostr_write(int nb, char **rows, int rc, int to_space)
{
	char	*key;
	int		result;

	key = itostr(nb);
	result = put_value(key, rows, rc, to_space);
	free(key);
	return (result);
}
