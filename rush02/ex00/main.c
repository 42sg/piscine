/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/08 11:40:58 by keyu              #+#    #+#             */
/*   Updated: 2023/07/08 13:59:32 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush02.h"

int	main(int argc, char **argv)
{
	long	parsed_l;

	if (argc >= 2 && argc <= 3)
	{
		parsed_l = custom_atol(argv[argc - 1]);
		if (parsed_l < 0 || parsed_l > 4294967295)
			put_str("Error\n");
		else
		{
			if (argc == 2)
				rush(0, (unsigned int)parsed_l);
			else
				rush(argv[1], (unsigned int)parsed_l);
		}
	}
	else
		put_str("Error\n");
	return (0);
}
