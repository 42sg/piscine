/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/08 12:21:52 by keyu              #+#    #+#             */
/*   Updated: 2023/07/08 12:37:19 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	is_space(char c)
{
	return (c == ' ' || (c >= 9 && c <= 13));
}

int	len(char *arr)
{
	int	i;

	i = 0;
	while (arr[i])
		i++;
	return (i);
}

long	custom_atol(char *arr)
{
	long	output;
	char	positivity;

	output = 0;
	positivity = 1;
	while (is_space(*arr))
		arr++;
	while (*arr == '+' || *arr == '-')
	{
		if (*arr == '-')
			positivity *= -1;
		arr++;
	}
	while (*arr >= '0' && *arr <= '9')
	{
		output *= 10;
		output += *arr++ - '0';
	}
	if (*arr != 0)
		return (-1);
	return (output * positivity);
}
