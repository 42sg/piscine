/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   triple.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wiltan <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/08 15:45:35 by wiltan            #+#    #+#             */
/*   Updated: 2023/07/08 19:45:24 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush02.h"

int	write_if_possible(int nb, t_parsed parsed, int ts, int prev_flag)
{
	if (prev_flag)
		return (itostr_write(nb, parsed.rows, parsed.rc, ts));
	return (0);
}

int	triple(int nb, t_parsed parsed)
{
	int	to_space;
	int	result;

	to_space = 0;
	if (nb >= 100)
	{
		result = write_if_possible((nb / 100), parsed, to_space, 1);
		to_space = 1;
		result = write_if_possible((100), parsed, to_space, result);
		nb -= (nb / 100) * 100;
	}
	if (nb >= 10 && nb < 20)
	{
		result = write_if_possible((nb), parsed, to_space, result);
		to_space = 1;
	}
	if (nb >= 20 && nb < 100)
	{
		result = write_if_possible((nb), parsed, to_space, result);
		to_space = 1;
		nb -= (nb / 10) * 10;
	}
	if (nb >= 1 && nb <= 9)
		result = write_if_possible((nb), parsed, to_space, result);
	return (result);
}
