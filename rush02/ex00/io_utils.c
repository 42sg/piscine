/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   io_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/08 16:17:44 by keyu              #+#    #+#             */
/*   Updated: 2023/07/08 19:47:19 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush02.h"

char	*get_file_contents(char *dict)
{
	int		fd;
	char	*buf;

	buf = malloc(2000 * sizeof(char));
	fd = open(dict, 0);
	read(fd, buf, 2000);
	close(fd);
	return (buf);
}

int	row_count(char *buf)
{
	int	rows;

	rows = 0;
	while (*buf)
	{
		if (*buf == '\n')
			rows++;
		buf++;
	}
	return (rows);
}

char	**allocate_row(char *buf)
{
	char	**rows;
	int		rc;
	int		index;
	int		pointer;

	rc = 0;
	pointer = 0;
	rows = malloc(row_count(buf) * sizeof(char **));
	while (rc < row_count(buf))
	{
		index = 0;
		while (buf[pointer] != '\n')
		{
			pointer++;
			index++;
		}
		pointer++;
		rows[rc] = malloc((index + 1) * sizeof(char));
		rc++;
	}
	return (rows);
}

void	duplicate_into_rows(char *file_contents, char **rows)
{
	int	rc;
	int	i;
	int	j;

	i = 0;
	rc = 0;
	while (rc < row_count(file_contents))
	{
		j = 0;
		while (file_contents[i] != '\n')
			rows[rc][j++] = file_contents[i++];
		rows[rc][j] = 0;
		i++;
		rc++;
	}
}

int	put_value(char *key, char **rows, int row_count, int to_space)
{
	int	row_p;
	int	i;
	int	j;

	row_p = 0;
	while (row_p < row_count)
	{
		i = 0;
		while (key[i] && key[i] == rows[row_p][i])
			i++;
		if (key[i] == 0 && (rows[row_p][i] == ' ' || rows[row_p][i] == ':'))
		{
			j = i;
			if (to_space)
				write(1, " ", 1);
			while (rows[row_p][j] == ':' || rows[row_p][j] == ' ')
				j++;
			while (rows[row_p][j] != 0)
				write(1, &rows[row_p][j++], 1);
			return (1);
		}
		row_p++;
	}
	put_str("Dict Error\n");
	return (0);
}
