| Level | Assignment |
| ----- | ---------- |
| 0 | aff_p |
| 1 | write_string |
| 2 | ft_strcmp |
| 3 | ft_strlen |
| 4 | ft_atoi |
| 5 | first_word |
| 6 | ft_putnbr |
| 7 | aff_first_param |
| 8 | last_word |
| 9 | count_words |
| 10 | ft_rrange |
| 11 | ft_split |
| 12 | ft_list_remove_if |
| 13 | count_alpha |
| 14 | str_maxlenoc |

14 * 6 = 84 pts

Comments:
- levels 0 - 11: finished in about 2 hours
- atoi, putnbr, first_word, last_word have logic that should be well understood
- stuck at 12 for 4 hours (somehow removing `free` works for me??)
- got to attempt 14 again

Learning points:
- figuring out syntax is never fun
  - in exam00 tried to figure how to pass program arguments for 2 hours
  - what is `void *` in level 12??
- don't give up

Closing:
As much as I do not like "practicing past exam questions", I think they build my foundation of logic and allows me to see things in a different perspective. I owe my result to exam questions posted online and looking at many different ways to solve the question. It is less of "I seen this question before so I can regurgitate the code", and more of "this is a very smart way, I could use this for this question too". Also, do not underestimate the time spent coding (whether for exam questions or not) for they build your confidence and fluency in the language (i.e. "how can I express this idea on code").
